# AngularTimer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

## Installation

Install the dependencies with ```npm i```

## Run
Run the mock server ```npm run json-server``` and then start the application with ```npm run start```

Or just run  ```npm run start-local``` for both.

## License

MIT
