import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Todo } from '../models/todo.model';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  urlService = 'http://localhost:3000/tasks/';

  constructor(private http: HttpClient) { }

  get(): Observable<Todo[]>{
    return this.http.get<any>(`${this.urlService}`);
  }

  delete(taskId: number): Observable<any>{
    return this.http.delete<any>(`${this.urlService}${taskId}`, {});
  }

  update(task: Todo): Observable<any>{
    return this.http.put<any>(`${this.urlService}${task.id}`, task);
  }

  create(task: Todo): Observable<any>{
    return this.http.post<any>(`${this.urlService}`, task);
  }
}
