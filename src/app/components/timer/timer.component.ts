import { Component, Input, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Todo } from 'src/app/models/todo.model';
import { secondsToMinutes, minsToSeconds } from '../../utils/timeUtils';
@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit, OnDestroy {
  @Input() task: Todo;
  @Output() updateTaskEvent = new EventEmitter<Todo>();

  loader: string;
  border: string;
  internalTicks = 0;
  isPlaying = false;
  intervalReference;
  isVisible = false;
  pi = 3.14;

  constructor() {}

  ngOnDestroy(): void {
    clearInterval(this.intervalReference);
  }

  ngOnInit(): void {
    this.internalTicks = this.task.ticks;
    if (this.task.ticks !== 0){
      this.draw(true);
    }
  }


  timeLeft = (): string => secondsToMinutes(this.internalTicks);

  setIsVisible = (visible: boolean): boolean => this.isVisible = visible;


  setIsPlaying(): void{
    if (this.isPlaying){
      // stop
      this.task.ticks = this.internalTicks;
      this.updateTaskEvent.emit(this.task);
      clearInterval(this.intervalReference);
    }else{
      // play
      this.draw();
      this.intervalReference = setInterval(() => this.draw(), 1000);
    }

    this.isPlaying = !this.isPlaying;
  }


  draw(init: boolean = false): void {
    const div = minsToSeconds(30);

    if (!init){
      this.internalTicks++;
    }
    this.internalTicks %= div;
    const r = (this.internalTicks * this.pi) / (div / 2);
    // console.log('play ', {id: this.taskId, ticks: this.ticks, internalTicks: this.internalTicks, r}  );
    const v = 125;
    const x = Math.sin(r) * v;
    const y = Math.cos(r) * -v;
    const mid = this.internalTicks > (div / 2) ? 1 : 0;

    const anim = `M 0 0 v -${v} A ${v} ${v} 1 ${mid} 1 ${x} ${y} z`;

    this.loader = anim;
    this.border = anim;

    if (this.internalTicks === 0){
      this.task.finished = true;
      this.task.ticks = 1800;
      this.updateTaskEvent.emit(this.task);
    }
  }
}
