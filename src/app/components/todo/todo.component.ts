import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Todo } from 'src/app/models/todo.model';
import { TodoService } from 'src/app/services/todo.service';
import { secondsToMinutes } from '../../utils/timeUtils';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  totalSpendTime: string;
  frmTodo: FormGroup;
  tasks: Todo[] = [];

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    this.frmTodo = new FormGroup({ taskName: new FormControl(null) });
    this.todoService.get().subscribe(
      res => this.tasks = res
    );
  }

  getTotalSpendTime(): string {
    const totalSpendTime = this.tasks?.length > 0 && this.tasks.filter(x => !x.finished);
    const reduce = totalSpendTime.length > 0 && totalSpendTime.map(x => x.ticks)?.reduce((acc, current) => acc += current);
    return secondsToMinutes(reduce);
  }

  removeTask(taskId: number): void{

    this.todoService.delete(taskId).subscribe(
      () => {
        this.tasks = this.tasks.filter(x => x.id !== taskId);
      },
      () => { alert('ocurró un error'); }
    );
  }

  createTask(): void {

    const copyTasks = JSON.parse(JSON.stringify(this.tasks));
    copyTasks.sort((a, b) => parseFloat(b.id) - parseFloat(a.id));
    const maxId = copyTasks[0] ? copyTasks[0].id + 1 : 1;
    const newTask: Todo = {
      id: maxId,
      description: this.frmTodo.value.taskName,
      finished: false,
      ticks: 0
    };
    this.todoService.create(newTask).subscribe(
      () => {
        this.tasks.push(newTask);
        this.frmTodo.reset();
      },
      () => { alert('ocurró un error'); }
    );
  }

  handleUpdateTaskEvent(task: Todo): void {

    this.todoService.update(task).subscribe(
      () => {
        if (task.finished) {
          this.tasks.find(x => x.id === task.id).finished = true;
        }
      },
      () => { alert('ocurró un error'); }
    );

  }


}
