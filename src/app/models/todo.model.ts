export interface Todo {
  id: number;
  description: string;
  ticks: number;
  finished: boolean;
}
